import 'package:dio/dio.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf.dart' as shelf;

import '../common/api.dart';
import 'character.service.dart';

final Dio client = Dio();

Future<shelf.Response> findCharacters(Request request) async {
  final limit = int.tryParse(request.getParam('limit') ?? '');
  final offset = int.tryParse(request.getParam('offset') ?? '');
  final nameStartsWith = request.getParam('nameStartsWith');

  return CharacterService(client).findCharacters(
    limit: limit,
    offset: offset,
    nameStartsWith: nameStartsWith,
  );
}
