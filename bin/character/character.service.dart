import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:shelf/shelf.dart' as shelf;

import '../common/api.dart';

class CharacterService {
  Dio? client;

  static final CharacterService _singleton = CharacterService._internal();

  factory CharacterService(Dio client) {
    _singleton.client ??= client;

    return _singleton;
  }

  CharacterService._internal();

  Future<shelf.Response> findCharacters({
    int? limit,
    int? offset,
    String? nameStartsWith,
  }) async {
    final params = {
      ...buildAuth().toMap(),
      if (limit != null) 'limit': limit,
      if (offset != null) 'offset': offset,
      if (nameStartsWith != null) 'nameStartsWith': nameStartsWith,
    };
    return client!
        .get(
      marvelApiCharactersEndpoint,
      queryParameters: params,
    )
        .then((response) {
      final json = response.data as Map<String, dynamic>;
      final resultDetail = {
        'offset': json['data']['offset'],
        'limit': json['data']['limit'],
        'total': json['data']['total'],
        'count': json['data']['count'],
      };
      final characters = _parseListWithMap(
        json['data']['results'] as List<dynamic>,
      );
      // final characters = _parseListWithOldFor(
      //   json['data']['results'] as List<dynamic>,
      // );
      // final characters = _parseListWithInlineFor(
      //   json['data']['results'] as List<dynamic>,
      // );
      final fullResponse = <String, dynamic>{
        'copyright': json['data']['copyright'],
        'attributionText': json['data']['attributionText'],
        'attributionHTML': json['data']['attributionHTML'],
        'resultDetail': resultDetail,
        'characters': characters,
      };
      return shelf.Response.ok(jsonEncode(fullResponse));
    }).catchError(_handleError);
  }

  List<Map<String, dynamic>> _parseListWithMap(List<dynamic> list) {
    return list
        .map((r) => r as Map<String, dynamic>)
        .map(
          (r) => {
            'id': r['id'],
            'name': r['name'],
            'description': r['description'],
            'thumbnail': {
              'standard':
                  '${r['thumbnail']['path']}/standard_fantastic.${r['thumbnail']['extension']}',
              'fullSize':
                  '${r['thumbnail']['path']}.${r['thumbnail']['extension']}',
            },
          },
        )
        .toList();
  }

  List<Map<String, dynamic>> _parseListWithOldFor(List<dynamic> list) {
    final characters = <Map<String, dynamic>>[];
    for (final c in list) {
      final character = c as Map<String, dynamic>;
      characters.add({
        'id': character['id'],
        'name': character['name'],
        'description': character['description'],
        'thumbnail': {
          'standard':
              '${character['thumbnail']['path']}/standard_fantastic.${character['thumbnail']['extension']}',
          'fullSize':
              '${character['thumbnail']['path']}.${character['thumbnail']['extension']}',
        }
      });
    }
    return characters;
  }

  List<Map<String, dynamic>> _parseListWithInlineFor(List<dynamic> list) {
    Map<String, dynamic> _parseCharacter(Map<String, dynamic> character) {
      return {
        'id': character['id'],
        'name': character['name'],
        'description': character['description'],
        'thumbnail': {
          'standard':
              '${character['thumbnail']['path']}/standard_fantastic.${character['thumbnail']['extension']}',
          'fullSize':
              '${character['thumbnail']['path']}.${character['thumbnail']['extension']}',
        }
      };
    }

    return [
      for (final c in list) _parseCharacter(c as Map<String, dynamic>),
    ];
  }

  shelf.Response _handleError(dynamic e) {
    if (e is DioError) {
      if (e.response != null) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx and is also not 304.
        return shelf.Response.internalServerError(
          body: {
            'data': e.response!.data,
            'headers': e.response!.headers,
            'requestOptions': e.response!.requestOptions,
          },
        );
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return shelf.Response.internalServerError(
          body: {
            'requestOptions': e.requestOptions,
            'message': e.message,
          },
        );
      }
    } else {
      return shelf.Response.internalServerError(body: e);
    }
  }
}
