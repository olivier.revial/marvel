import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:shelf/shelf.dart';

import 'utils.dart';

bool isTest = false;

const marvelBaseApi = 'http://gateway.marvel.com/v1/public';
const marvelApiCharactersEndpoint = '$marvelBaseApi/characters';

final marvelApiPublicKey = Platform.environment['MARVEL_PUBLIC_KEY'];
final marvelApiPrivateKey = Platform.environment['MARVEL_PRIVATE_KEY'];

class AuthParams implements JsonSerializable {
  final String apiKey;
  final String hash;
  final String ts;

  AuthParams(this.apiKey, this.hash, this.ts);

  @override
  Map<String, Object> toMap() => {
        'apikey': apiKey,
        'hash': hash,
        'ts': ts,
      };
}

AuthParams buildAuth() {
  if (!isTest) {
    assert(
      marvelApiPrivateKey != null && marvelApiPrivateKey!.isNotEmpty,
      'You must set MARVEL_PRIVATE_KEY variable',
    );
    assert(
      marvelApiPublicKey != null && marvelApiPublicKey!.isNotEmpty,
      'You must set MARVEL_PUBLIC_KEY variable',
    );
  }

  final ts = DateTime.now().millisecondsSinceEpoch.toString();
  final hash = md5
      .convert(utf8.encode('$ts$marvelApiPrivateKey$marvelApiPublicKey'))
      .toString();
  return AuthParams(marvelApiPublicKey ?? '', hash, ts);
}

extension RequestUtils on Request {
  String? getParam(String name) {
    return url.queryParameters[name];
  }
}
