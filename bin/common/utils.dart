abstract class JsonSerializable {
  Map<String, dynamic> toMap();
}
