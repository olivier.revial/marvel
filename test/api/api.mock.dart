const marvelApiResultMock = <String, dynamic>{
  "data": {
    "offset": 0,
    "limit": 1,
    "total": 1504,
    "count": 1,
    "results": [
      {
        "id": "001",
        "name": "Wouki Man",
        "description": "Best MCU character ever",
        "modified": "2014-04-29T14:18:17-0400",
        "thumbnail": {"path": "http://thumbnail", "extension": "jpg"},
        "resourceURI": "http://gateway.marvel.com/v1/public/characters/001",
        "comics": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/comics",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/comics/001",
              "name": "Avengers: The Initiative (2007) #14"
            }
          ],
          "returned": 1
        },
        "series": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/series",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/series/001",
              "name": "Marvel Premiere (1972 - 1981)"
            }
          ],
          "returned": 1
        },
        "stories": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/stories",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/stories/001",
              "name":
                  "Avengers: The Initiative (2007) #14, Spotlight Variant - Int",
              "type": "interiorStory"
            }
          ],
          "returned": 1
        },
        "events": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/events",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/events/001",
              "name": "Secret Invasion"
            }
          ],
          "returned": 1
        },
        "urls": [
          {"type": "detail", "url": "http://marvel.com/characters/001"}
        ]
      },
      {
        "id": "002",
        "name": "PommeDouze",
        "description": "An awesome character indeed",
        "modified": "2015-04-29T14:18:17-0400",
        "thumbnail": {"path": "http://thumbnail2", "extension": "jpg"},
        "resourceURI": "http://gateway.marvel.com/v1/public/characters/002",
        "comics": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/comics",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/comics/001",
              "name": "Avengers: The Initiative (2007) #14"
            }
          ],
          "returned": 1
        },
        "series": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/series",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/series/001",
              "name": "Marvel Premiere (1972 - 1981)"
            }
          ],
          "returned": 1
        },
        "stories": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/stories",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/stories/001",
              "name":
                  "Avengers: The Initiative (2007) #14, Spotlight Variant - Int",
              "type": "interiorStory"
            }
          ],
          "returned": 1
        },
        "events": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/events",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/events/001",
              "name": "Secret Invasion"
            }
          ],
          "returned": 1
        },
        "urls": [
          {"type": "detail", "url": "http://marvel.com/characters/002"}
        ]
      },
      {
        "id": "003",
        "name": "John Doe",
        "description": "Oh, this is random",
        "modified": "2016-04-29T14:18:17-0400",
        "thumbnail": {"path": "http://thumbnail3", "extension": "jpg"},
        "resourceURI": "http://gateway.marvel.com/v1/public/characters/003",
        "comics": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/comics",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/comics/001",
              "name": "Avengers: The Initiative (2007) #14"
            }
          ],
          "returned": 1
        },
        "series": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/series",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/series/001",
              "name": "Marvel Premiere (1972 - 1981)"
            }
          ],
          "returned": 1
        },
        "stories": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/stories",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/stories/001",
              "name":
                  "Avengers: The Initiative (2007) #14, Spotlight Variant - Int",
              "type": "interiorStory"
            }
          ],
          "returned": 1
        },
        "events": {
          "available": 1,
          "collectionURI":
              "http://gateway.marvel.com/v1/public/characters/001/events",
          "items": [
            {
              "resourceURI": "http://gateway.marvel.com/v1/public/events/001",
              "name": "Secret Invasion"
            }
          ],
          "returned": 1
        },
        "urls": [
          {"type": "detail", "url": "http://marvel.com/characters/003"}
        ]
      }
    ]
  }
};
