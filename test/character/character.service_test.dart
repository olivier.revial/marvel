import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:test/test.dart';

import '../../bin/character/character.service.dart';
import '../../bin/common/api.dart';
import '../api/api.mock.dart';

void main() {
  final dio = Dio();
  final dioAdapater = DioAdapter(dio: dio);
  final characterService = CharacterService(dio);

  setUp(() {
    // Needed to avoid checking environment variables while doing tests
    isTest = true;
  });

  test('calls Marvel API without parameters by default', () async {
    // Given
    dioAdapater.onGet(
      marvelApiCharactersEndpoint,
      (request) => request.reply(200, marvelApiResultMock),
    );

    // When
    final response = await characterService
        .findCharacters()
        .then((value) => value.readAsString())
        .then(jsonDecode)
        .then((json) => json as Map<String, dynamic>);

    // Then
    expect(response['characters'] as List<dynamic>, hasLength(3));

    final _firstCharacter = (response['characters'] as List<dynamic>).first;
    expect(_firstCharacter['id'], '001');
    expect(_firstCharacter['name'], 'Wouki Man');
  });

  test('calls Marvel API with given parameters', () async {
    // Given
    dioAdapater.onGet(
      marvelApiCharactersEndpoint,
      (request) => request.reply(200, marvelApiResultMock),
      queryParameters: {
        'limit': '2',
        'offset': '10',
        'nameStartsWith': 'Test',
      },
    );

    // When
    final response = await characterService
        .findCharacters(
          limit: 2,
          offset: 10,
          nameStartsWith: 'Test',
        )
        .then((value) => value.readAsString())
        .then(jsonDecode)
        .then((json) => json as Map<String, dynamic>);

    // Then
    expect(response['characters'] as List<dynamic>, hasLength(3));
  });
}
