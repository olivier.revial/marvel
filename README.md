# Marvel

Simple Dart server to play with Dart a little.

## Technical stacks

- Dart 2.17
- Shelf package (HTTP router)
- Dio package (HTTP client)

## Setup environment variable

```shell
export MARVEL_PUBLIC_KEY=<Your marvel public key>
export MARVEL_PRIVATE_KEY=<Your marvel private key>
```

## How to run

### Development mode

```shell
dart bin/server.dart
// Or
dart run bin/server.dart
```

### Binary mode

To generate a self-contained executable :

```shell
dart compile exe bin/server.dart -o  build/release
./build/release
```

Or to generate an AOT release :

```shell
dart compile aot-snapshot bin/server.dart -o build/aot-release
chmod +x build/aot-release
dartaotruntime build/aot-release
```